<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
   <title>Spring MVC Example</title>
</head>
<body>
   <div class="container mt-5">
      <div class="row">
         <div class="col-12">
            <form action="hello">
               <div class="form-group">
                  <label for="hello">Input Text</label>
                  <input type="text" name="hello" placeholder="Type your text..." id="hello" class="form-control">
               </div>
               <button class="btn btn-primary mt-3">Submit</button>
            </form>
         </div>
      </div>
   </div>
   <script src="./assets/js/bootstrap.min.js"></script>
</body>
</html>